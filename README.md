# Cashboard

## Projects

### Database

The [Database](database/README.md) project contains tools to manage database schema and migrations

### Libs

| Name                        | Description                                                            |
|-----------------------------|------------------------------------------------------------------------|
| [nest](libs/nest/README.md) | A collection of [NestJS](nestjs.com) modules                           |
| [ng](libs/ng/README.md)     | A collection of [Angular](https://angular.io/) components and services |

### Apps

| Name                                  | Description                 |
|---------------------------------------|-----------------------------|
| [admin](apps/admin/README.md) | The admin dashboard for CCR |

### Services

| Name                          | Description                 |
|-------------------------------|-----------------------------|
| [api](services/api/README.md) | The admin dashboard for CCR |
