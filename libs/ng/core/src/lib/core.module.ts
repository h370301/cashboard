import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from './layout';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
  ],
  exports: [
    LayoutModule,
  ],
})
export class CoreModule {}
