import { Component } from '@angular/core';

@Component({
  selector: 'cbd-ng-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
})
export class PageComponent {}
