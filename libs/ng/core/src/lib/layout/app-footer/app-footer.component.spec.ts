import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppFooterComponent } from './app-footer.component';
import { MteNavFooterWrapper } from '@mortar/angular/elements/atomic/nav';
import { MteHebWrapper } from '@mortar/angular/elements/bespoke/heb';

describe('AppFooterComponent', () => {
  let component: AppFooterComponent;
  let fixture: ComponentFixture<AppFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppFooterComponent],
      imports: [
        MteNavFooterWrapper,
        MteHebWrapper,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
