import { Component, Input } from '@angular/core';

@Component({
  selector: 'cbd-ng-page-section',
  templateUrl: './page-section.component.html',
  styleUrls: ['./page-section.component.scss'],
})
export class PageSectionComponent {
  /**
   * Allow the section to breakout of the width of the container and take up
   * the full width of the page
   */
  @Input()
  public set breakout(value: boolean | string) {
    this.fullWidth = value != null && `${value}` !== 'false';
  }

  /**
   * Helper attribute to hold fullWidth value as a coerced boolean
   */
  public fullWidth = false;
}
