import { NgModule } from '@angular/core';
import { AppWrapperComponent } from './app-wrapper/app-wrapper.component';
import {
  MteNavContentWrapper,
  MteNavFooterWrapper,
  MteNavHeaderWrapper,
  MteNavRootWrapper,
} from '@mortar/angular/elements/atomic/nav';
import { MteStackWrapper } from '@mortar/angular/elements/atomic/stack';
import { AppContentComponent } from './app-content/app-content.component';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { PageComponent } from './page/page.component';
import { PageSectionComponent } from './page-section/page-section.component';
import { MteHebWrapper } from '@mortar/angular/elements/bespoke/heb';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppContentComponent,
    AppFooterComponent,
    AppHeaderComponent,
    AppWrapperComponent,
    PageComponent,
    PageSectionComponent,
  ],
  imports: [
    CommonModule,
    MteNavRootWrapper,
    MteStackWrapper,
    MteNavFooterWrapper,
    MteHebWrapper,
    MteNavHeaderWrapper,
    MteNavContentWrapper
  ],
  exports: [
    AppContentComponent,
    AppFooterComponent,
    AppHeaderComponent,
    AppWrapperComponent,
    PageComponent,
    PageSectionComponent,
  ],
})
export class LayoutModule {}
