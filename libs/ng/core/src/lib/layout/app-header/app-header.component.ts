import { Component } from '@angular/core';

@Component({
  selector: 'cbd-ng-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
})
export class AppHeaderComponent {}
