import { Component } from '@angular/core';

@Component({
  selector: 'cbd-ng-app-wrapper',
  templateUrl: './app-wrapper.component.html',
  styleUrls: ['./app-wrapper.component.scss'],
})
export class AppWrapperComponent {}
