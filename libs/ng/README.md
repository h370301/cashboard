# ng

A collection of [Angular](https://angular.io/) components and services

## Entrypoints

| Name                                           | Description                                                            |
|------------------------------------------------|------------------------------------------------------------------------|
| `@cashboard/ng`                                | A collection of [Angular](https://angular.io/) components and services |
| [`@cashboard/ng/core`](libs/ng/core/README.md) | Core components for setting up the base structure of an application    |

## Components

(Imported from `@cashboard/ng`)

#### PrettyPrintJson

This can be used to debug json by putting it into a readable format on the page.

```html
<!-- json: the actual json object to print -->
<!-- showNumberLine: begins each line w/ a number (defaults to true) -->
<!-- padding: amount of padding to prepend (defaults to 4)  -->
<cbd-ng-pretty-print-json
  [json]="{ key: true }"
  [showNumberLine]="true"
  padding="4"
/>
```
