import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cbd-ng-pretty-print-json',
  templateUrl: './pretty-print-json.component.html',
  styleUrls: ['./pretty-print-json.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom,
})
export class PrettyPrintJsonComponent {
  @Input() json: unknown = {};
  @Input() showNumberLine = true;
  @Input() padding = 4;
}
