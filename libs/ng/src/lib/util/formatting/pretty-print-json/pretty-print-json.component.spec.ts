import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PrettyPrintJsonComponent } from './pretty-print-json.component';

describe('PrettyPrintJsonComponent', () => {
  let component: PrettyPrintJsonComponent;
  let fixture: ComponentFixture<PrettyPrintJsonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrettyPrintJsonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PrettyPrintJsonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
