import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailStackComponent } from './user-detail-stack/user-detail-stack.component';
import { MteTitleWrapper } from '@mortar/angular/elements/atomic/title';
import { GravatarComponent } from './gravatar/gravatar.component';
import { MteImageWrapper } from '@mortar/angular/elements/atomic/image';
import { MteStackWrapper } from '@mortar/angular/elements/atomic/stack';
import { MteTextWrapper } from '@mortar/angular/elements/atomic/text';

@NgModule({
  imports: [
    CommonModule,
    MteTitleWrapper,
    MteImageWrapper,
    MteStackWrapper,
    MteTextWrapper,
  ],
  declarations: [
    UserDetailStackComponent,
    GravatarComponent,
  ],
  exports: [
    UserDetailStackComponent,
    GravatarComponent,
  ]
})
export class UserModule {}
