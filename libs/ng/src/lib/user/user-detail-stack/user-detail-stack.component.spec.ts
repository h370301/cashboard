import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserDetailStackComponent } from './user-detail-stack.component';

describe('UserDetailStackComponent', () => {
  let component: UserDetailStackComponent;
  let fixture: ComponentFixture<UserDetailStackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserDetailStackComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(UserDetailStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
