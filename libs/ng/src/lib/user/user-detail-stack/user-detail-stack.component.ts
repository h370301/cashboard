import { Component, Input } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'cbd-ng-user-detail-stack',
  templateUrl: './user-detail-stack.component.html',
  styleUrls: ['./user-detail-stack.component.scss'],
})
export class UserDetailStackComponent {
  @Input()
  public userDetail: User | null = null;
}
