export interface Supplier {
  name: string;
  apNumber: number;
}

export interface User {
  name: string;
  email: string;
  emailHash?: string;
  supplier: Supplier | null;
}
