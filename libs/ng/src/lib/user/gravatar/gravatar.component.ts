import { Component, Input } from '@angular/core';

@Component({
  selector: 'cbd-ng-gravatar',
  templateUrl: './gravatar.component.html',
  styleUrls: ['./gravatar.component.scss'],
})
export class GravatarComponent {
  public gravatarUrl?: string;
  @Input()
  public set email(value: string) {
    this.gravatarUrl = `https://gravatar.com/avatar/${value}?d=identicon`;
  }
  @Input()
  public rounded = true;
}
