import { Component } from '@angular/core';

@Component({
  selector: 'heb-ng-app-content',
  templateUrl: './app-content.component.html',
  styleUrls: ['./app-content.component.scss'],
})
export class AppContentComponent {}
