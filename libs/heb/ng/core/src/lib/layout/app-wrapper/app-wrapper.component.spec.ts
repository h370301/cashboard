import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppWrapperComponent } from './app-wrapper.component';
import { MteNavRootWrapper } from '@mortar/angular/elements/atomic/nav';
import { MteStackWrapper } from '@mortar/angular/elements/atomic/stack';

describe('AppWrapperComponent', () => {
  let component: AppWrapperComponent;
  let fixture: ComponentFixture<AppWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppWrapperComponent],
      imports: [
        MteNavRootWrapper,
        MteStackWrapper,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
