import { Component, Input } from '@angular/core';

@Component({
  selector: 'heb-ng-app-footer',
  templateUrl: './app-footer.component.html',
  styleUrls: ['./app-footer.component.scss'],
})
export class AppFooterComponent {
  /**
   * Show a copyright at the bottom of the footer
   */
  @Input()
  public set copyright(value: boolean | string) {
    this.showCopyright = value != null && `${value}` !== 'false';
  }

  public showCopyright: boolean = false;

  public copyrightYear: number = new Date().getFullYear();
}
