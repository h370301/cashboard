import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { AppHeaderComponent } from './app-header.component';
import { MteNavRootWrapper } from '@mortar/angular/elements/atomic/nav';
import { MteStackWrapper } from '@mortar/angular/elements/atomic/stack';

const meta: Meta<AppHeaderComponent> = {
  component: AppHeaderComponent,
  title: 'AppHeaderComponent',
  decorators: [
    moduleMetadata({
      imports: [
        MteNavRootWrapper,
        MteStackWrapper,
      ],
    }),
  ],
};
export default meta;
type Story = StoryObj<AppHeaderComponent>;

export const Default: Story = {
  args: {},
};
