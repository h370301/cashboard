# @cashboard/heb-ng/core

Secondary entry point of `@cashboard/heb-ng`. It can be used by importing from `@cashboard/heb-ng/core`.

## Setting up

You will still need to set up mortar and import all the styles/modules as described by their [docs](mortar.heb.com).

```shell
pnpm add @mortar/{angular,styles} @angular/cdk
```

A base application wrapper can be set up using the following template:

```html
<cbd-ng-app-wrapper>
  <cbd-ng-app-header />
  <cbd-ng-app-content>
    <router-outlet></router-outlet>
  </cbd-ng-app-content>
  <cbd-ng-app-footer copyright />
</cbd-ng-app-wrapper>
```

For a page, wrap everything in a `PageComponent` and/or `PageSection` component

```html
<cbd-ng-page>
  <cbd-ng-page-section>
    Contained content
  </cbd-ng-page-section>
  <cbd-ng-page-section breakout>
    Full with content
  </cbd-ng-page-section>
  <cbd-ng-page-section>
    Contained content
  </cbd-ng-page-section>
</cbd-ng-page>
```
