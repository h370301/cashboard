# heb-ng

A collection of [Angular](https://angular.io/) components and services

## Entrypoints

| Name                                                   | Description                                                            |
|--------------------------------------------------------|------------------------------------------------------------------------|
| `@cashboard/heb-ng`                                    | A collection of [Angular](https://angular.io/) components and services |
| [`@cashboard/heb-ng/core`](libs/heb/ng/core/README.md) | Core components for setting up the base structure of an application    |
