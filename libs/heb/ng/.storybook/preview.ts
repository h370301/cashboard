import { applicationConfig, Preview } from '@storybook/angular';
import { provideAnimations } from '@angular/platform-browser/animations';
import { importProvidersFrom } from '@angular/core';
import { MtrElementsInteropModule } from '@mortar/angular/components/core';
import { MtrIconModule } from '@mortar/angular/components/atomic/icon';
import { MtrToastModule } from '@mortar/angular/components/atomic/toast';

const preview: Preview = {
  decorators: [
    applicationConfig({
      providers: [
        provideAnimations(),
        importProvidersFrom(MtrElementsInteropModule.forRoot()),
        importProvidersFrom(MtrIconModule.forRoot()),
        importProvidersFrom(MtrToastModule.forRoot()),
      ],
    }),
  ],
};
