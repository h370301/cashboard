import { ApiProperty } from '@nestjs/swagger';

export class BaseApiResponse<T> {
  @ApiProperty({ type: Number })
  public statusCode!: number;

  public data!: T;
}
