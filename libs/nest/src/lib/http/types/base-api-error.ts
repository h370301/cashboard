import { ApiProperty } from '@nestjs/swagger';

export class BaseApiError {
  @ApiProperty({ type: String })
  public message!: string;

  @ApiProperty({ type: String })
  public errorName!: string;

  @ApiProperty({ type: Object })
  public details!: unknown;

  @ApiProperty({ type: String })
  public path!: string;

  @ApiProperty({ type: String })
  public timestamp!: string;
}
