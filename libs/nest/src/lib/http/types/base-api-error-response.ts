import { ApiProperty } from '@nestjs/swagger';
import { BaseApiError } from './base-api-error';

export class BaseApiErrorResponse {
  @ApiProperty({ type: Number })
  public statusCode!: number;

  @ApiProperty({ type: BaseApiError })
  public error!: BaseApiError;
}
