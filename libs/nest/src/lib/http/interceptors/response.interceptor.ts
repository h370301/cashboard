import {
  CallHandler,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { catchError, map, Observable, of } from 'rxjs';
import { BaseApiResponse } from '../types';
import { BaseApiErrorResponse } from '../types';

@Injectable()
export class ResponseInterceptor<T = unknown> implements NestInterceptor {
  public intercept(
    context: ExecutionContext,
    next: CallHandler<T>,
  ): Observable<BaseApiResponse<T> | BaseApiErrorResponse> {
    const ctx = context.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    return next.handle()
      .pipe(
        map((data) => ({
          statusCode: parseInt(response.statusCode),
          data,
        })),
        catchError((error: Error) => {
          const status = error instanceof HttpException
            ? error.getStatus()
            : HttpStatus.INTERNAL_SERVER_ERROR;
          console.log(error.stack);

          return of({
            statusCode: status,
            error: {
              message: error.message,
              errorName: error.name,
              details: error.stack,
              path: request.url,
              timestamp: new Date().toISOString(),
            },
          });
        }),
      );
  }
}
