# @mashboard/nest/sequelize

A collection of tools for connecting to and managing databases using [Sequelize](https://sequelize.org/)

## Usage

**Note:** You need to install `@sequelize/core` and `sqlite3` (or whatever db you're using) in any app you will be
using this module.

Create a strategy for setting up the database.

```typescript
import { SequelizeStrategy } from '@cashboard/nest/sequelize';
import { UserModel } from '../users/user.model';

export const CoreDatabaseStrategy = new SequelizeStrategy({
  dialect: 'sqlite',
  storage: ':memory:',
  models: [UserModel],
});
```

```typescript
// app.module.ts
import { Module } from '@nestjs/common';
import { DatabaseModule } from '@cashboard/nest/sequelize';
import { CoreDatabaseStrategy } from './core-database.strategy';

@Module({
  imports: [
    DatabaseModule.forRoot({
      strategy: CoreDatabaseStrategy,
    }),
  ],
})
export class AppModule {}
```

To register models in the scope of a specific module, use `DatabaseModule.forFeature` and define
which models you want to include.

```typescript
// user.module.ts
import { Module } from '@nestjs/common';
import { DatabaseModule } from '@cashboard/nest/sequelize';
import { ModelStatic as SequelizeModelStatic } from '@sequelize/core';
import { UserModel } from './user.model';
import { UserService } from './user.service';
import { CoreDatabaseStrategy } from './core-database.strategy';

@Module({
  imports: [
    DatabaseModule.forFeature<SequelizeModelStatic>(
      [UserModel],
      { strategy: CoreDatabaseStrategy },
    ),
  ],
  providers: [UserService],
})
export class UserModule {}
```

You can then inject the model into another class.

```typescript
import { Injectable } from '@nestjs/common';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';
import { UserModel } from './user.model';

@Injectable()
export class UserService {
  constructor(
    @InjectSequelizeModel(User)
    private usersRepository: typeof UserModel,
  ) {}
}
```
