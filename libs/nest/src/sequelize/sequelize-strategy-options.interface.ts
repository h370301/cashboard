import { Options as SequelizeOptions } from '@sequelize/core';

export type SequelizeStrategyOptions = {
  database: SequelizeOptions;
};
