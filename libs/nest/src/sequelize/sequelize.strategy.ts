import { DatabaseStrategy } from '../database';
import {
  ModelStatic as SequelizeModelStatic,
  Sequelize,
} from '@sequelize/core';
import { SequelizeStrategyOptions } from './sequelize-strategy-options.interface';

export class SequelizeStrategy implements DatabaseStrategy<Sequelize, SequelizeModelStatic> {
  public readonly config: SequelizeStrategyOptions;
  public readonly sequelize!: Sequelize;

  constructor(config: SequelizeStrategyOptions) {
    this.config = config;

    this.sequelize = new Sequelize(this.config.database);
  }

  public onApplicationShutdown(): Promise<void> {
    return this.sequelize.close();
  }

  public static getModelToken(model: SequelizeModelStatic): string {
    return model.name;
  }

  public getModelToken(model: SequelizeModelStatic): string {
    return SequelizeStrategy.getModelToken(model);
  }

  public createConnectionFactory(): Promise<Sequelize> {
    return this.sequelize.authenticate()
      .then(() => this.sequelize);
  }
}
