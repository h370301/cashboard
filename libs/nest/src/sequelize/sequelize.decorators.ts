import { ModelStatic as SequelizeModelStatic } from '@sequelize/core';
import { Inject } from '@nestjs/common';
import { SequelizeStrategy } from './sequelize.strategy';
import { InjectDatabaseModel } from '../database';

export const InjectSequelizeModel: InjectDatabaseModel<SequelizeModelStatic> = (model) => {
  return Inject(SequelizeStrategy.getModelToken(model));
};
