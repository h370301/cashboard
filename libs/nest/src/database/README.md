# database

## Usage

**Note:** You need to install `@sequelize/core` and `sqlite3` (or whatever db you're using) in any app you will be
using this module.

Create a strategy for setting up the database.

```typescript
import { SequelizeStrategy } from './sequelize.strategy';
import { UserModel } from '../users/user.model';

export const CoreDatabaseStrategy = new SequelizeStrategy({
  dialect: 'sqlite',
  storage: ':memory:',
  models: [UserModel],
});
```

Then add the `DatabaseModule` with the strategy to the `imports` section of your AppModule.

```typescript
import { Module } from '@nestjs/common';
import { DatabseModule } from '@mashboard/nest/database';
import { CoreDatabaseStrategy } from './core-database.strategy';

@Module({
  imports: [
    DatabaseModule.forRoot({
      strategy: CoreDatabaseStrategy,
    }),
  ],
})
export class AppModule {}
```

To register models in the scope of a specific module, use `DatabaseModule.forFeature` and define
which models you want to include.

```typescript
import { Module } from '@nestjs/common';
import { DatabseModule } from '@mashboard/nest/database';
import { DatabaseModule } from './database.module';
import { ModelStatic as SequelizeModelStatic } from '@sequelize/core';
import { UserModel } from './user.model';
import { UsersService } from './users.service';
import { CoreDatabaseStrategy } from './core-database.strategy';

@Module({
  imports: [
    DatabaseModule.forFeature<SequelizeModelStatic>(
      [UserModel],
      { strategy: CoreDatabaseStrategy },
    ),
  ],
  providers: [UsersService],
})
export class UsersModule {}
```

You can then inject the model into another class.

```typescript
import { Injectable } from '@nestjs/common';
import { InjectSequelizeModel } from '@mashboard/nest/database';
import { UserModel } from './user.model';

@Injectable()
export class UsersService {
  constructor(
    @InjectSequelizeModel(User)
    private usersRepository: typeof UserModel,
  ) {}
}
```
