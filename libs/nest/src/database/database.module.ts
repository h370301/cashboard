import { DynamicModule, Module, Provider, Type } from '@nestjs/common';
import { DATABASE, DATABASE_MODULE_OPTIONS } from './database.constants';
import {
  DatabaseModuleAsyncOptions,
  DatabaseModuleOptions,
  DatabaseOptionsFactory,
  DatabaseStrategy
} from './interfaces';

@Module({})
export class DatabaseModule {
  static forRoot(options: DatabaseModuleOptions): DynamicModule {
    const databaseModuleOptions: Provider = {
      provide: DATABASE_MODULE_OPTIONS,
      useValue: options,
    };
    const connectionProvider = {
      provide: DATABASE,
      useFactory: () => options.strategy.createConnectionFactory(options),
    };

    return {
      module: DatabaseModule,
      providers: [
        databaseModuleOptions,
        connectionProvider,
      ],
      exports: [
        connectionProvider,
      ],
    };
  }

  static forFeature<BaseModel = any>(models: BaseModel[], strategy: DatabaseStrategy<any, any>): DynamicModule {
    const modelProviders = models.map((model) => {
      return {
        provide: strategy.getModelToken(model),
        useValue: model,
      };
    });

    return {
      module: DatabaseModule,
      providers: [
        ...modelProviders,
      ],
      exports: [
        ...modelProviders,
      ],
    };
  }

  static forRootAsync(options: DatabaseModuleAsyncOptions): DynamicModule {
    const connectionProvider = {
      provide: DATABASE,
      useFactory: (databaseModuleOptions: DatabaseModuleOptions) => {
        return databaseModuleOptions.strategy.createConnectionFactory(databaseModuleOptions);
      },
      inject: [DATABASE_MODULE_OPTIONS],
    };
    const asyncProviders = this.createAsyncProviders(options);

    return {
      module: DatabaseModule,
      imports: options.imports,
      providers: [
        ...asyncProviders,
        connectionProvider,
      ],
      exports: [
        connectionProvider,
      ],
    };
  }

  private static createAsyncProviders(options: DatabaseModuleAsyncOptions): Provider[] {
    if (options.useExisting || options.useFactory) {
      return [
        this.createAsyncOptionsProviders(options),
      ];
    }

    const useClass = options.useClass as Type<DatabaseOptionsFactory>;

    return [
      this.createAsyncOptionsProviders(options),
      {
        provide: useClass,
        useClass,
      },
    ];
  }


  private static createAsyncOptionsProviders(options: DatabaseModuleAsyncOptions): Provider {
    if (options.useFactory) {
      return {
        provide: DATABASE_MODULE_OPTIONS,
        useFactory: options.useFactory,
        inject: options.inject || [],
      };
    }

    return {
      provide: DATABASE_MODULE_OPTIONS,
      useFactory: (databaseOptionsFactory: DatabaseOptionsFactory) => {
        return databaseOptionsFactory.createDatabaseOptions();
      },
      inject: [
        (options.useClass || options.useExisting) as Type<DatabaseOptionsFactory>,
      ],
    }
  }
}
