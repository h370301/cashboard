import { DatabaseStrategy } from './database-strategy.interface';
import { ModuleMetadata, Type } from '@nestjs/common';

export interface DatabaseModuleOptions {
  strategy: DatabaseStrategy<any, any>;
}

export interface DatabaseOptionsFactory {
  createDatabaseOptions(): Promise<DatabaseModuleOptions> | DatabaseModuleOptions;
}

export interface DatabaseModuleAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  useExisting?: Type<DatabaseOptionsFactory>;
  useClass?: Type<DatabaseOptionsFactory>;
  useFactory?: (...args: any[]) => Promise<DatabaseModuleOptions> | DatabaseModuleOptions;
  inject?: any[];
}
