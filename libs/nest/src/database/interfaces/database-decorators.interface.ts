import { Inject } from '@nestjs/common';

export type InjectDatabaseModel<BaseModel> = (model: BaseModel) => ReturnType<typeof Inject>;
