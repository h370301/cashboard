import { OnApplicationShutdown } from '@nestjs/common';
import { DatabaseModuleOptions } from './database-module-options.interface';

export interface DatabaseStrategy<Connection extends {}, BaseModel> extends OnApplicationShutdown {
  getModelToken(model: BaseModel): string;
  createConnectionFactory(options: DatabaseModuleOptions): Promise<Connection> | Connection;
}
