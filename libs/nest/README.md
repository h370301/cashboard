# nest

## Entrypoints

| Name                                                  | Description                                                                                        |
|-------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| `@mashboard/nest`                                     | Currently empty                                                                                    |
| [`@mashboard/nest/database`](src/database/README.md)  | A collection of tools to connect to and manage databases                                           |
| [`@mashboard/nest/sequelize`](src/database/README.md) | A collection of tools to connect to and manage databases using [Sequelize](https://sequelize.org/) |
