import { Route } from '@angular/router';

export const appRoutes: Route[] = [
  {
    path: 'submissions',
    loadChildren: () => import('./submissions/submissions.module')
      .then((module) => module.SubmissionsModule),
  },
  {
    path: '**',
    redirectTo: '/submissions',
  },
];
