import { Component } from '@angular/core';
import { CcrApiService } from '../shared/ccr/ccr-api.service';
import {
  MtrRemotePageRequest,
  MtrRemotePageResult,
  MtrRemotePaginatedTableDataSource
} from '@mortar/angular/components/atomic/table';
import { Submission } from '../shared/ccr/models/submission.model';
import { map, Observable } from 'rxjs';
import { SubmissionRow } from '../shared/submissions-table/submission-row';

@Component({
  selector: 'cbd-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
})
export class OverviewComponent {
  public submissionSource = new MtrRemotePaginatedTableDataSource<SubmissionRow>({
    requester: this.fetchSubmissionPageFactory(),
    initialPageIndex: 0,
    initialPageSize: 10,
  });

  constructor(
    private ccrApiService: CcrApiService,
  ) {}

  private fetchSubmissionPageFactory() {
    return (
      req: MtrRemotePageRequest<Submission>,
    ): Observable<MtrRemotePageResult<SubmissionRow>> => {
      console.log('req.pageIndex', req.pageIndex);
      return this.ccrApiService.listCostChangeRequests(req.pageIndex)
        .pipe(
          // map the rows to be SubmissionForm
          map(({ data }) => ({
            ...data,
            data: data.rows.map((row) => ({
              id: row.id,
              type: row.formType,
              name: row.displayName,
              contact: row.username,
              apNumber: row.apNumber,
              supplier: 'row.ccrUsersSupplier.apDescription',
              selectionCount: 3, // 'row.ccrSelections.length',
              dealCount: 2, //'row.ccrDeals.length',
            })),
          })),
          // map the response to be MtrRemotePageResult
          map((res) => ({
            data: res.data,
            totalItems: res.total,
            pageIndex: res.page,
            pageSize: res.limit,
          }))
        );
    }
  }
}
