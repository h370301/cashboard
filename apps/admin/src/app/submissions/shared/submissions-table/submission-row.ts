export interface SubmissionRow {
  id: number | string;
  type: string;
  name: string;
  contact: string;
  apNumber: string | number;
  supplier: string;
  selectionCount: number;
  dealCount: number;
}
