import { Component, Input } from '@angular/core';
import { MtrRemotePaginatedTableDataSource } from '@mortar/angular/components/atomic/table';
import { SubmissionRow } from './submission-row';

@Component({
  selector: 'cbd-submissions-table',
  templateUrl: './submissions-table.component.html',
  styleUrls: ['./submissions-table.component.scss'],
})
export class SubmissionsTableComponent {
  @Input() data!: MtrRemotePaginatedTableDataSource<SubmissionRow>;
  public columns = [
    'id', 'type', 'name', 'contact', 'apNumber',
    'supplier', 'selectionCount', 'dealCount',
  ];

  public trackById(idx: number, submission: SubmissionRow): string | number {
    return submission.id;
  }
}
