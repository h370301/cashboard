import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CcrApiPaginatedResponse } from './ccr-api-response.interface';
import { Submission } from './models/submission.model';

@Injectable({
  providedIn: 'root'
})
export class CcrApiService {
  public readonly rootUrl = '/api';

  constructor(
    private httpClient: HttpClient,
  ) {}

  public listCostChangeRequests(
    page = 0,
  ): Observable<CcrApiPaginatedResponse<Submission>> {
    const endpoint = `${this.rootUrl}/cost-change-request/paginate`;
    const params = new HttpParams().set('page', page);

    return this.httpClient.get<CcrApiPaginatedResponse<Submission>>(endpoint, {
      params,
    });
  }
}
