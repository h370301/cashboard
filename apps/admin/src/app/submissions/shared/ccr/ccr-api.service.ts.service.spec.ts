import { TestBed } from '@angular/core/testing';

import { CcrApiService } from './ccr-api.service';

describe('CcrApiServiceTsService', () => {
  let service: CcrApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CcrApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
