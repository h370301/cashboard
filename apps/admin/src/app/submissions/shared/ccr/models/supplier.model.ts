export interface Supplier {
  createdAt: Date;
  id: number;
  userId: number;
  apNumber: number;
  apDescription: string;
  updatedAt: Date;
}
