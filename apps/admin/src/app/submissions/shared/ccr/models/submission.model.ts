import { User } from './user.model';
import { Supplier } from './supplier.model';
import { Bda } from './bda.model';
import { Bdm } from './bdm.model';
import { Contact } from './contact.model';

export interface Submission {
  abandoned: boolean;
  apNumber: number;
  createdAt: Date;
  displayName: string;
  formType: string;
  id: number;
  updatedAt: Date;
  userId: string;
  username: string;
  ccrUser: User;
  ccrUsersSupplier: Supplier;
  ccrBdaEmailRecipients: Bda[];
  ccrBdmEmailRecipients: Bdm[];
  ccrContacts: Contact;
  // TODO fill out model for ccrSelections
  ccrSelections: unknown[];
  // TODO fill out model for ccrDeals
  ccrDeals: unknown[];
}
