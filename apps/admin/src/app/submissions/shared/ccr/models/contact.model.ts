export interface Contact {
  costJustificationFormId: number;
  createdAt: Date;
  email: string;
  id: number;
  name: string;
  phoneNumber: string;
  phoneNumberExt: string;
  updatedAt: Date;
}
