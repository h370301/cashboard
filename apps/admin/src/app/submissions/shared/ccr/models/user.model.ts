import { Supplier } from './supplier.model';

export interface User {
  createdAt: Date;
  displayName: string;
  email: string;
  id: number;
  isAuthorized: boolean;
  sidmId: string;
  updatedAt: Date;
  userName: string;
  ccrUsersSuppliers: Supplier[];
}
