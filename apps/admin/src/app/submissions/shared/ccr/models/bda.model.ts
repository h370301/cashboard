export interface Bda {
  bdmEmailRecipientsId: number;
  costJustificationFormId: number;
  createdAt: Date;
  email: string;
  id: number;
  selectionId: number;
  updatedAt: Date;
}
