export interface Bdm {
  code: string;
  costJustificationFormId: number;
  createdAt: Date;
  dealId: number;
  email: string;
  firstName: string;
  fullName: string;
  isShareableEmail: boolean;
  lastName: string;
  selectionId: number;
  updatedAt: Date;
}
