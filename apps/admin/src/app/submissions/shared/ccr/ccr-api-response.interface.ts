export interface CcrApiPage<T> {
  rows: T extends (infer U)[] ? U : T[];
  page: number;
  count: number;
  total: number;
  limit: number;
}

export interface CcrApiError {
  timestamp: Date;
  message: string;
}

export interface CcrApiResponse<T> {
  data: T;
  error: CcrApiError | null;
}

export type CcrApiPaginatedResponse<T> = CcrApiResponse<CcrApiPage<T>>;
