import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubmissionsRoutingModule } from './submissions-routing.module';
import { OverviewComponent } from './overview/overview.component';
import { SharedModule } from '../../shared/shared.module';
import { CcrApiService } from './shared/ccr/ccr-api.service';
import { HttpClientModule } from '@angular/common/http';
import { MtrTableModule } from '@mortar/angular/components/atomic/table';
import { MtrPaginatorModule } from '@mortar/angular/components/atomic/paginator';
import { SubmissionsTableComponent } from './shared/submissions-table/submissions-table.component';

@NgModule({
  declarations: [
    OverviewComponent,
    SubmissionsTableComponent,
  ],
  imports: [
    CommonModule,
    SubmissionsRoutingModule,
    SharedModule,
    HttpClientModule,
    MtrTableModule,
    MtrPaginatorModule,
  ],
  providers: [
    CcrApiService,
  ],
})
export class SubmissionsModule { }
