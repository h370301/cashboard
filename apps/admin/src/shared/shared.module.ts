import { NgModule } from '@angular/core';
import { MtrElementsInteropModule } from '@mortar/angular/components/core';
import { MtrIconModule } from '@mortar/angular/components/atomic/icon';
import { MtrToastModule } from '@mortar/angular/components/atomic/toast';
import { CoreModule } from '@cashboard/heb-ng/core';

@NgModule({
  imports: [
    MtrElementsInteropModule.forRoot(),
    MtrIconModule.forRoot(),
    MtrToastModule.forRoot(),
    CoreModule,
  ],
  exports: [
    CoreModule,
  ],
})
export class SharedModule {}
