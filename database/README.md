# database

A collection of tools to manage databases

## Targets

| Name      | Example                                             | Description                                                                                                |
|-----------|-----------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| migrate   | `nx run database:migrate $DB_NAME`                  | Run all the migrations in the `database/config/$DB_NAME/migrations` directory                              |
| rollback  | `nx run database:rollback $DB_NAME`                 | Rolls back all the migrations in the `database/config/$DB_NAME/migrations` directory                       |
| generate  | `nx run database:generate $DB_NAME $MIGRATION_NAME` | Generates new migrations at `database/config/$DB_NAME/migrations/$TIMESTAMP_$MIGRATION_NAME/{up,down}.sql` |
| pending   | `nx run database:pending $DB_NAME`                  | List all migrations that have not been applied to `$DB_NAME`                                               |
| complete  | `nx run database:completed $DB_NAME`                | List all migrations that have been applied to `$DB_NAME`                                                   |


