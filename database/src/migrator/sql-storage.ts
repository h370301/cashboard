import { MigrationParams, UmzugStorage } from 'umzug';
import { RawSqlClient } from './client';

export interface SqlStorageOptions {
  tableName: string;
}

export class SqlStorage implements UmzugStorage<RawSqlClient> {
  public readonly tableName: string;

  constructor(options: SqlStorageOptions) {
    this.tableName = options.tableName;
  }

  public logMigration(params: MigrationParams<RawSqlClient>): Promise<void> {
    return Promise.resolve()
      .then(() => {
        params.context.query(`insert into ${this.tableName} values ($1)`, [ params.name ])
      });
  }

  public unlogMigration(params: MigrationParams<RawSqlClient>): Promise<void> {
    return Promise.resolve()
      .then(() => {
        params.context.query(`delete from ${this.tableName} where name = $1`, [ params.name ]);
      });
  }

  public executed(params: Pick<MigrationParams<RawSqlClient>, 'context'>): Promise<string[]> {
    return params.context.query(`create table if not exists ${this.tableName}(name text)`)
      .then(() => params.context.query(`select name from ${this.tableName}`))
      .then(([values]) => (values as Array<{ name: string }>).map((v: { name: string }) => v.name));
  }
}
