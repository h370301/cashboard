import { Options as SequelizeOptions, Sequelize } from '@sequelize/core';

export interface RawSqlClient {
  query: (sql: string, values?: unknown[]) => Promise<[unknown[], unknown]>
}

export const getRawSqlClient = (config: SequelizeOptions): RawSqlClient => {
  const sequelize = new Sequelize(config);

  return {
    query: (sql: string, values?: unknown[])  => sequelize.query(sql, { bind: values }),
  };
}
