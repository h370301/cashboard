import { Umzug } from 'umzug';
import * as fs from 'fs';
import { RawSqlClient, getRawSqlClient } from './client';
import { SqlStorage } from './sql-storage';
import * as path from 'path';

export interface MigratorOptions {
  databaseName: string;
  databaseRoot: string;
}

export function createMigratorFactory(config: MigratorOptions): Umzug<RawSqlClient> {
  return new Umzug({
    migrations: {
      glob: `${config.databaseRoot}/migrations/**/up.sql`,
      resolve: (params) => {
        const migrationDir = path.dirname(`${params.path}`);
        const migrationName = `${migrationDir.split('/').at(-1)}`;
        const upPath = path.join(migrationDir, 'up.sql');
        const downPath = path.join(migrationDir, 'down.sql');

        return {
          name: migrationName,
          up: () => params.context.query(fs.readFileSync(upPath).toString()),
          down: () => params.context.query(fs.readFileSync(downPath).toString()),
        };
      },
    },
    context: getRawSqlClient({
      dialect: 'sqlite',
      storage: path.join(config.databaseRoot, 'database.sqlite'),
    }),
    storage: new SqlStorage({
      tableName: 'migration_meta',
    }),
    logger: console,
  });
}
