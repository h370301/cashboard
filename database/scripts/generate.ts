import * as process from 'process';
import * as path from 'node:path';
import * as fs from 'fs';

const [migrationName, databaseName] = process.argv.slice(2);

if (!migrationName || !databaseName) {
  console.error('Missing database and/or migration name - ex) migrate.ts $DB_NAME $MIGRATION_NAME\n');

  process.exit(1);
}

const databaseRoot = path.resolve('database/config', databaseName);

(() => {
  const now = Date.now();
  const migrationDir = path.join(databaseRoot, 'migrations', `${now}_${migrationName}`);

  // Avoid over-writing an existing migration
  // This will almost never happen due to the timestamp but you never know
  if (fs.existsSync(migrationDir)) {
    console.error(`Migration already exists at ${migrationDir}`);

    process.exit(1);
  }

  fs.mkdirSync(migrationDir);
  fs.openSync(path.join(migrationDir, 'up.sql'), 'w');
  fs.openSync(path.join(migrationDir, 'down.sql'), 'w');
})();
