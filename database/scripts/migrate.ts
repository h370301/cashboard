import { createMigratorFactory } from '../src/migrator';
import * as path from 'node:path';

if (process.argv.length <= 3) {
  console.error('Missing database name - ex) migrate.ts up|down $DB_NAME\n');

  process.exit(1);
}

const databaseName = `${process.argv.pop()}`.toLowerCase();
const databaseRoot = path.resolve('database/config', databaseName);

const migrator = createMigratorFactory({
  databaseName,
  databaseRoot,
});

migrator.runAsCLI();
