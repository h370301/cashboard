# api

The api service for CCR

## Setup

Before starting the API service, create an environment file with values for connecting to the database

These are the same values that we use for the local db in CCR. The models were implemented a while ago
on a previous version of the db schema so there may be some wonkiness

```
export CCR_DATABASE_HOST="$VALUE"
export CCR_DATABASE_PORT="$VALUE"
export CCR_DATABASE_USERNAME="$VALUE"
export CCR_DATABASE_PASSWORD="$VALUE"
export CCR_DATABASE_DATABASE="$VALUE"
```

After this file is created, run `source $FILEPATH` and then you can run `nx serve api`

You can see these environment variables being applied in [ccr-database.strategy.ts](src/ccr/shared/strategies/ccr-database.strategy.ts)
