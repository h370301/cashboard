import { Injectable } from '@nestjs/common';
import { ImpactAndSolutionsModel } from './models/impact-and-solutions.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class ImpactAndSolutionsService extends BaseService<ImpactAndSolutionsModel>{
  constructor(
    @InjectSequelizeModel(ImpactAndSolutionsModel)
    private readonly repository: BaseModelStatic<ImpactAndSolutionsModel>,
  ) {
    super(repository);
  }
}
