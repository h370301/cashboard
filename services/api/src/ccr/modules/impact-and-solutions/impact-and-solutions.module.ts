import { Module } from '@nestjs/common';
import { ImpactAndSolutionsModel } from './models/impact-and-solutions.model';
import { ImpactAndSolutionsService } from './impact-and-solutions.service';
import { ImpactAndSolutionsController } from './impact-and-solutions.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([ImpactAndSolutionsModel], CcrDatabaseStrategy)],
  providers: [ImpactAndSolutionsService],
  controllers: [ImpactAndSolutionsController],
})
export class ImpactAndSolutionsModule {}
