import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';
import { Type } from 'class-transformer';

export class ImpactAndSolutions {
  public costChangePreventionActionsTaken: string;

  @Type(() => CostChangeRequest)
  public costChangeRequest: CostChangeRequest;

  public costJustificationFormId: number;

  public createdAt: Date;

  public hebMitigationPlan: string;

  public id: number;

  public isPromotionOrDealsIncluded: boolean;

  public selectionId: number;

  public updatedAt: Date;
}
