import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { ImpactAndSolutionsModel } from './models/impact-and-solutions.model';
import { ImpactAndSolutionsService } from './impact-and-solutions.service';

@Controller('impact-and-solutions')
@ApiTags('Impact And Solutions')
export class ImpactAndSolutionsController extends BaseController<ImpactAndSolutionsModel>() {
  constructor(
    private readonly impactAndSolutionsService: ImpactAndSolutionsService,
  ) {
    super(impactAndSolutionsService);
  }
}
