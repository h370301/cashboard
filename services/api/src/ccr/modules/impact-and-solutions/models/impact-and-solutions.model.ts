import { CreationOptional, DataTypes, NonAttribute } from '@sequelize/core';
import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { CostChangeRequestModel } from '../../cost-change-request/models/cost-change-request.model';

@Table({
  tableName: 'ccr_impact_and_solutions',
})
export class ImpactAndSolutionsModel extends BaseModel<ImpactAndSolutionsModel> {
  @Attribute(DataTypes.STRING)
  @NotNull()
  declare costChangePreventionActionsTaken: string;

  declare costChangeRequest: NonAttribute<CostChangeRequestModel>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare costJustificationFormId: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.STRING)
  declare hebMitigationPlan: string;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.BOOLEAN)
  declare isPromotionOrDealsIncluded: boolean;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare selectionId: number;

  declare updatedAt: CreationOptional<Date>;
}
