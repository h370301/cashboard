import { Injectable } from '@nestjs/common';
import { ItemDetailWarehouseModel } from './models/item-detail-warehouse.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class ItemDetailWarehouseService extends BaseService<ItemDetailWarehouseModel>{
  constructor(
    @InjectSequelizeModel(ItemDetailWarehouseModel)
    private readonly repository: BaseModelStatic<ItemDetailWarehouseModel>,
  ) {
    super(repository);
  }
}
