import { CreationOptional, DataTypes } from '@sequelize/core';
import { Attribute, AutoIncrement, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { BaseModel } from '../../../../shared/http/classes/base.model';

@Table({
  tableName: 'ccr_item_details_warehouses',
})
export class ItemDetailWarehouseModel extends BaseModel<ItemDetailWarehouseModel> {
  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.INTEGER)
  declare itemDetailId: number;

  declare updatedAt: CreationOptional<Date>;

  @Attribute(DataTypes.INTEGER)
  declare warehouseNumber: number;
}
