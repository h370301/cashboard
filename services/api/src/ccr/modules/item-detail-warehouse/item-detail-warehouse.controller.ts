import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { ItemDetailWarehouseModel } from './models/item-detail-warehouse.model';
import { ItemDetailWarehouseService } from './item-detail-warehouse.service';

@Controller('item-detail-warehouse')
@ApiTags('Item Detail Warehouse')
export class ItemDetailWarehouseController extends BaseController<ItemDetailWarehouseModel>() {
  constructor(
    private readonly itemDetailWarehouseService: ItemDetailWarehouseService,
  ) {
    super(itemDetailWarehouseService);
  }
}
