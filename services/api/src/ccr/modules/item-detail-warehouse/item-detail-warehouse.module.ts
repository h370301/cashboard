import { Module } from '@nestjs/common';
import { ItemDetailWarehouseModel } from './models/item-detail-warehouse.model';
import { ItemDetailWarehouseService } from './item-detail-warehouse.service';
import { ItemDetailWarehouseController } from './item-detail-warehouse.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([ItemDetailWarehouseModel], CcrDatabaseStrategy)],
  providers: [ItemDetailWarehouseService],
  controllers: [ItemDetailWarehouseController],
})
export class ItemDetailWarehouseModule {}
