import { Module } from '@nestjs/common';
import { UserModel } from './models/user.model';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([UserModel], CcrDatabaseStrategy)],
  providers: [UserService],
  controllers: [UserController],
})
export class UserModule {}
