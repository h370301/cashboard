import { Injectable } from '@nestjs/common';
import { UserModel } from './models/user.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class UserService extends BaseService<UserModel>{
  constructor(
    @InjectSequelizeModel(UserModel)
    private readonly repository: BaseModelStatic<UserModel>,
  ) {
    super(repository);
  }
}
