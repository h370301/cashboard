import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { UserModel } from './models/user.model';
import { UserService } from './user.service';

@Controller('user')
@ApiTags('User')
export class UserController extends BaseController<UserModel>() {
  constructor(
    private readonly userService: UserService,
  ) {
    super(userService);
  }
}
