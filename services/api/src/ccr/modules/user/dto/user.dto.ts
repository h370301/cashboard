import crypto from 'crypto';
import { Expose, Type } from 'class-transformer';
import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';
import { Supplier } from '../../supplier/dto/supplier.dto';

export class User {
  @Type(() => CostChangeRequest)
  public costChangeRequests: CostChangeRequest[];

  public displayName: string;

  public email: string;

  @Expose()
  public get emailHash(): string {
    return crypto.createHash('md5')
      .update(this.email)
      .digest('hex');
  }
  public isAuthorized: boolean;

  public id: number;

  public sidmId: string;

  public userName: string;

  @Type(() => Supplier)
  public usersSuppliers?: Supplier[];
}
