import { CreationOptional, DataTypes, NonAttribute } from '@sequelize/core';
import {
  Attribute,
  AutoIncrement,
  HasMany,
  NotNull,
  PrimaryKey,
  Table,
} from '@sequelize/core/decorators-legacy';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { CostChangeRequestModel } from '../../cost-change-request/models/cost-change-request.model';
import { SupplierModel } from '../../supplier/models/supplier.model';

@Table({
  tableName: 'ccr_users',
})
export class UserModel extends BaseModel<UserModel> {
  declare createdAt: CreationOptional<Date>;

  declare costChangeRequests?: NonAttribute<CostChangeRequestModel[]>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare displayName: string;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare email: string;

  @Attribute(DataTypes.BOOLEAN)
  @NotNull()
  declare isAuthorized: boolean;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare sidmId: string;

  declare updatedAt: CreationOptional<Date>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare userName: string;

  @HasMany(() => SupplierModel, {
    foreignKey: 'userId',
    inverse: {
      as: 'users'
    },
  })
  declare usersSuppliers?: NonAttribute<SupplierModel[]>;
}
