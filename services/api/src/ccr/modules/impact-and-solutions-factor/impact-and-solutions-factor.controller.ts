import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { ImpactAndSolutionsFactorModel } from './models/impact-and-solutions-factor.model';
import { ImpactAndSolutionsFactorService } from './impact-and-solutions-factor.service';

@Controller('impact-and-solutions-factor')
@ApiTags('Impact And Solutions Factor')
export class ImpactAndSolutionsFactorController extends BaseController<ImpactAndSolutionsFactorModel>() {
  constructor(
    private readonly impactAndSolutionsFactorService: ImpactAndSolutionsFactorService,
  ) {
    super(impactAndSolutionsFactorService);
  }
}
