import { Module } from '@nestjs/common';
import { ImpactAndSolutionsFactorModel } from './models/impact-and-solutions-factor.model';
import { ImpactAndSolutionsFactorService } from './impact-and-solutions-factor.service';
import { ImpactAndSolutionsFactorController } from './impact-and-solutions-factor.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([ImpactAndSolutionsFactorModel], CcrDatabaseStrategy)],
  providers: [ImpactAndSolutionsFactorService],
  controllers: [ImpactAndSolutionsFactorController],
})
export class ImpactAndSolutionsFactorModule {}
