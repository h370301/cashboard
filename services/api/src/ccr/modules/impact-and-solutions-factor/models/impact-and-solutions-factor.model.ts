import { CreationOptional, DataTypes } from '@sequelize/core';
import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { BaseModel } from '../../../../shared/http/classes/base.model';

@Table({
  tableName: 'ccr_impact_and_solutions_factors',
})
export class ImpactAndSolutionsFactorModel extends BaseModel<ImpactAndSolutionsFactorModel> {
  @Attribute(DataTypes.INTEGER)
  declare additionalContextId: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare factorPercentage: number;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare impactAndSolutionsId: number;

  @Attribute(DataTypes.STRING)
  declare reasonCode: string;

  declare updatedAt: CreationOptional<Date>;
}
