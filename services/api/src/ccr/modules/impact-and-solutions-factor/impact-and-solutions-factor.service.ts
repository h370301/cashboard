import { Injectable } from '@nestjs/common';
import { ImpactAndSolutionsFactorModel } from './models/impact-and-solutions-factor.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class ImpactAndSolutionsFactorService extends BaseService<ImpactAndSolutionsFactorModel>{
  constructor(
    @InjectSequelizeModel(ImpactAndSolutionsFactorModel)
    private readonly repository: BaseModelStatic<ImpactAndSolutionsFactorModel>,
  ) {
    super(repository);
  }
}
