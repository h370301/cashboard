import { Module } from '@nestjs/common';
import { ContactModel } from './models/contact.model';
import { ContactService } from './contact.service';
import { ContactController } from './contact.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([ContactModel], CcrDatabaseStrategy)],
  providers: [ContactService],
  controllers: [ContactController],
})
export class ContactModule {}
