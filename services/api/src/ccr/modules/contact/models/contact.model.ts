import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { CreationOptional, DataTypes } from '@sequelize/core';
import { BaseModel } from '../../../../shared/http/classes/base.model';

@Table({
  tableName: 'ccr_contacts',
})
export class ContactModel extends BaseModel<ContactModel> {
  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare costJustificationFormId: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare email: string;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare name: string;

  @Attribute(DataTypes.STRING)
  declare phoneNumber: string;

  @Attribute(DataTypes.STRING)
  declare phoneNumberExt: string;

  declare updatedAt: CreationOptional<Date>;
}
