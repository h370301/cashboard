import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { ContactModel } from './models/contact.model';
import { ContactService } from './contact.service';

@Controller('contact')
@ApiTags('Contact')
export class ContactController extends BaseController<ContactModel>() {
  constructor(
    private readonly contactService: ContactService,
  ) {
    super(contactService);
  }
}
