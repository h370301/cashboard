import { Injectable } from '@nestjs/common';
import { ContactModel } from './models/contact.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class ContactService extends BaseService<ContactModel>{
  constructor(
    @InjectSequelizeModel(ContactModel)
    private readonly repository: BaseModelStatic<ContactModel>,
  ) {
    super(repository);
  }
}
