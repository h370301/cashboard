import { Type } from 'class-transformer';
import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';

export class Contact {
  public costJustificationFormId: number;

  @Type(() => CostChangeRequest)
  public costChangeRequest: CostChangeRequest;

  public createdAt: Date;

  public email: string;

  public id: number;

  public name: string;

  public phoneNumber: string;

  public phoneNumberExt: string;

  public updatedAt: Date;
}
