import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';
import { Type } from 'class-transformer';

export class Bda {
  public bdmEmailRecipientsId: number;

  @Type(() => CostChangeRequest)
  public costChangeRequest: CostChangeRequest;

  public costJustificationFormId: number;

  public createdAt: Date;

  public email: string;

  public id: number;

  public selectionId: number;

  public updatedAt: Date;
}
