import { CreationOptional, DataTypes, NonAttribute } from '@sequelize/core';
import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { CostChangeRequestModel } from '../../cost-change-request/models/cost-change-request.model';

@Table({
  tableName: 'ccr_bda_email_recipients',
})
export class BdaModel extends BaseModel<BdaModel> {
  @Attribute(DataTypes.INTEGER)
  declare bdmEmailRecipientsId: number;

  declare costChangeRequest: NonAttribute<CostChangeRequestModel>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare costJustificationFormId: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare email: string;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare selectionId: number;

  declare updatedAt: CreationOptional<Date>;
}
