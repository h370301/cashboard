import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { BdaModel } from './models/bda.model';
import { BdaService } from './bda.service';

@Controller('bda')
@ApiTags('BDA')
export class BdaController extends BaseController<BdaModel>() {
  constructor(
    private readonly bdaService: BdaService,
  ) {
    super(bdaService);
  }
}
