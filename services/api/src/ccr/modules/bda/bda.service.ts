import { Injectable } from '@nestjs/common';
import { BdaModel } from './models/bda.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class BdaService extends BaseService<BdaModel>{
  constructor(
    @InjectSequelizeModel(BdaModel)
    private readonly repository: BaseModelStatic<BdaModel>,
  ) {
    super(repository);
  }
}
