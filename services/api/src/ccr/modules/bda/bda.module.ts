import { Module } from '@nestjs/common';
import { BdaModel } from './models/bda.model';
import { BdaService } from './bda.service';
import { BdaController } from './bda.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([BdaModel], CcrDatabaseStrategy)],
  providers: [BdaService],
  controllers: [BdaController],
})
export class BdaModule {}
