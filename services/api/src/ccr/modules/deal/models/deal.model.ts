import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { CreationOptional, DataTypes } from '@sequelize/core';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { SelectionType } from '../../../shared/constants';

@Table({
  tableName: 'ccr_deals',
})
export class DealModel extends BaseModel<DealModel> {
  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare costJustificationFormId: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare dealAmount: string;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare dealOutline: string;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare dealType: string;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare dealUnit: string;

  @Attribute(DataTypes.DATE)
  declare endDate: Date;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare selection: string;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare selectionType: SelectionType;

  @Attribute(DataTypes.DATE)
  @NotNull()
  declare startDate: Date;

  declare updatedAt: CreationOptional<Date>;
}
