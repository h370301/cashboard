import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { DealModel } from './models/deal.model';
import { DealService } from './deal.service';

@Controller('deal')
@ApiTags('Deal')
export class DealController extends BaseController<DealModel>() {
  constructor(
    private readonly dealService: DealService,
  ) {
    super(dealService);
  }
}
