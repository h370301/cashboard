import { Injectable } from '@nestjs/common';
import { DealModel } from './models/deal.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class DealService extends BaseService<DealModel>{
  constructor(
    @InjectSequelizeModel(DealModel)
    private readonly repository: BaseModelStatic<DealModel>,
  ) {
    super(repository);
  }
}
