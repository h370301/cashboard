import { SelectionType } from '../../../shared/constants';
import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';
import { Type } from 'class-transformer';

export class Deal {
  public costJustificationFormId: number;

  @Type(() => CostChangeRequest)
  public costChangeRequest: CostChangeRequest;

  public createdAt: Date;

  public dealAmount: string;

  public dealOutline: string;

  public dealType: string;

  public dealUnit: string;

  public endDate: Date;

  public id: number;

  public selection: string;

  public selectionType: SelectionType;

  public startDate: Date;

  public updatedAt: Date;
}
