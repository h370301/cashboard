import { Module } from '@nestjs/common';
import { DealModel } from './models/deal.model';
import { DealService } from './deal.service';
import { DealController } from './deal.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([DealModel], CcrDatabaseStrategy)],
  providers: [DealService],
  controllers: [DealController],
})
export class DealModule {}
