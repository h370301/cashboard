import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';
import { Type } from 'class-transformer';

export class ReasonForChange {
  @Type(() => CostChangeRequest)
  declare costChangeRequest: CostChangeRequest;

  declare costJustificationFormId: number;

  declare createdAt: Date;

  declare id: number;

  declare reasonCode: string;

  declare selectionId: number;

  declare updatedAt: Date;
}
