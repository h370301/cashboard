import { Module } from '@nestjs/common';
import { ReasonForChangeModel } from './models/reason-for-change.model';
import { ReasonForChangeService } from './reason-for-change.service';
import { ReasonForChangeController } from './reason-for-change.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([ReasonForChangeModel], CcrDatabaseStrategy)],
  providers: [ReasonForChangeService],
  controllers: [ReasonForChangeController],
})
export class ReasonForChangeModule {}
