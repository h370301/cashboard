import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { ReasonForChangeModel } from './models/reason-for-change.model';
import { ReasonForChangeService } from './reason-for-change.service';

@Controller('reason-for-change')
@ApiTags('Reason For Change')
export class ReasonForChangeController extends BaseController<ReasonForChangeModel>() {
  constructor(
    private readonly reasonForChangeService: ReasonForChangeService,
  ) {
    super(reasonForChangeService);
  }
}
