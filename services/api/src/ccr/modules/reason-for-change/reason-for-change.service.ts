import { Injectable } from '@nestjs/common';
import { ReasonForChangeModel } from './models/reason-for-change.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class ReasonForChangeService extends BaseService<ReasonForChangeModel>{
  constructor(
    @InjectSequelizeModel(ReasonForChangeModel)
    private readonly repository: BaseModelStatic<ReasonForChangeModel>,
  ) {
    super(repository);
  }
}
