import { Injectable } from '@nestjs/common';
import { IngredientAdditionalContextModel } from './models/ingredient-additional-context.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class IngredientAdditionalContextService extends BaseService<IngredientAdditionalContextModel>{
  constructor(
    @InjectSequelizeModel(IngredientAdditionalContextModel)
    private readonly repository: BaseModelStatic<IngredientAdditionalContextModel>,
  ) {
    super(repository);
  }
}
