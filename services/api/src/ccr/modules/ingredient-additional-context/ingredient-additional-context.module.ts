import { Module } from '@nestjs/common';
import { IngredientAdditionalContextModel } from './models/ingredient-additional-context.model';
import { IngredientAdditionalContextService } from './ingredient-additional-context.service';
import { IngredientAdditionalContextController } from './ingredient-additiona-context.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([IngredientAdditionalContextModel], CcrDatabaseStrategy)],
  providers: [IngredientAdditionalContextService],
  controllers: [IngredientAdditionalContextController],
})
export class IngredientAdditionalContextModule {}
