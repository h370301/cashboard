import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { IngredientAdditionalContextModel } from './models/ingredient-additional-context.model';
import { IngredientAdditionalContextService } from './ingredient-additional-context.service';

@Controller('ingredient-additional-context')
@ApiTags('Ingredient Additional Context')
export class IngredientAdditionalContextController extends BaseController<IngredientAdditionalContextModel>() {
  constructor(
    private readonly ingredentAdditionalContextService: IngredientAdditionalContextService,
  ) {
    super(ingredentAdditionalContextService);
  }
}
