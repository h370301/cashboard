import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';
import { Type } from 'class-transformer';

export class IngredientAdditionalContext {
  public additionalDetail: string;

  public affectedIngredient: string;

  public causeDetail: string;

  public changeCause: string;

  public changeSource: string;

  // @Attribute(DataTypes.VIRTUAL(DataTypes.STRING, []))
  // public get contextType(): NonAttribute<string> {
  //   return 'INGREDIENTS';
  // };

  @Type(() => CostChangeRequest)
  public costChangeRequest: CostChangeRequest;

  public costJustificationFormId: number;

  public createdAt: Date;

  public dateExperienced: Date;

  public id: number;

  public quantityPercentRangeEnd: number;

  public quantityPercentExact: number;

  public quantityPercentRangeStart: number;

  public selectionId: number;

  public updatedAt: Date;
}
