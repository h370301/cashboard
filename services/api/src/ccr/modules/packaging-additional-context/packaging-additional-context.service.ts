import { Injectable } from '@nestjs/common';
import { PackagingAdditionalContextModel } from './models/packaging-additional-context.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class PackagingAdditionalContextService extends BaseService<PackagingAdditionalContextModel>{
  constructor(
    @InjectSequelizeModel(PackagingAdditionalContextModel)
    private readonly repository: BaseModelStatic<PackagingAdditionalContextModel>,
  ) {
    super(repository);
  }
}
