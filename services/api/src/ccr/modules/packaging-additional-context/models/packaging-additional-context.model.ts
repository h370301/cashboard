import { CreationOptional, DataTypes, NonAttribute } from '@sequelize/core';
import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { CostChangeRequestModel } from '../../cost-change-request/models/cost-change-request.model';

@Table({
  tableName: 'ccr_additional_context_packaging',
})
export class PackagingAdditionalContextModel extends BaseModel<PackagingAdditionalContextModel> {
  @Attribute(DataTypes.STRING)
  declare additionalDetail: string;

  @Attribute(DataTypes.STRING)
  declare affectedMaterial: string;

  @Attribute(DataTypes.STRING)
  declare causeDetail: string;

  @Attribute(DataTypes.STRING)
  declare changeCause: string;

  @Attribute(DataTypes.STRING)
  declare causeMaterial: string;

  @Attribute(DataTypes.STRING)
  declare changeSource: string;

  // @Attribute(DataTypes.VIRTUAL(DataTypes.STRING, []))
  // public get contextType(): NonAttribute<string> {
  //   return 'PACKAGING';
  // };

  declare costChangeRequest: NonAttribute<CostChangeRequestModel>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare costJustificationFormId: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.DATE)
  declare dateExperienced: Date;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.INTEGER)
  declare quantityPercentRangeEnd: number;

  @Attribute(DataTypes.INTEGER)
  declare quantityPercentExact: number;

  @Attribute(DataTypes.INTEGER)
  declare quantityPercentRangeStart: number;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare selectionId: number;

  declare updatedAt: CreationOptional<Date>;
}
