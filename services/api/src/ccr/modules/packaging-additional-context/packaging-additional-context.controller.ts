import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { PackagingAdditionalContextModel } from './models/packaging-additional-context.model';
import { PackagingAdditionalContextService } from './packaging-additional-context.service';

@Controller('packaging-additional-context')
@ApiTags('Packaging Additional Context')
export class PackagingAdditionalContextController extends BaseController<PackagingAdditionalContextModel>() {
  constructor(
    private readonly packagingAdditionalContextService: PackagingAdditionalContextService,
  ) {
    super(packagingAdditionalContextService);
  }
}
