import { Module } from '@nestjs/common';
import { PackagingAdditionalContextModel } from './models/packaging-additional-context.model';
import { PackagingAdditionalContextService } from './packaging-additional-context.service';
import { PackagingAdditionalContextController } from './packaging-additional-context.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([PackagingAdditionalContextModel], CcrDatabaseStrategy)],
  providers: [PackagingAdditionalContextService],
  controllers: [PackagingAdditionalContextController],
})
export class PackagingAdditionalContextModule {}
