import { CreationOptional, DataTypes, NonAttribute } from '@sequelize/core';
import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { CostChangeRequestModel } from '../../cost-change-request/models/cost-change-request.model';

@Table({
  tableName: 'ccr_bdm_email_recipients'
})
export class BdmModel extends BaseModel<BdmModel> {
  @Attribute(DataTypes.STRING)
  declare code: string;

  declare costChangeRequest: NonAttribute<CostChangeRequestModel>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare costJustificationFormId: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.INTEGER)
  declare dealId: number;

  @Attribute(DataTypes.STRING)
  declare email: string;

  @Attribute(DataTypes.STRING)
  declare firstName: string;

  @Attribute(DataTypes.STRING)
  declare fullName: string;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.BOOLEAN)
  @NotNull()
  declare isShareableEmail: boolean;

  @Attribute(DataTypes.STRING)
  declare lastName: string;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare selectionId: number;

  declare updatedAt: CreationOptional<Date>;
}
