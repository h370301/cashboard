import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';
import { Type } from 'class-transformer';

export class Bdm {
  public code: string;

  @Type(() => CostChangeRequest)
  public costChangeRequest: CostChangeRequest;

  public costJustificationFormId: number;

  public createdAt: Date;

  public dealId: number;

  public email: string;

  public firstName: string;

  public fullName: string;

  public id: number;

  public isShareableEmail: boolean;

  public lastName: string;

  public selectionId: number;

  public updatedAt: Date;
}
