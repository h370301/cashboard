import { Module } from '@nestjs/common';
import { BdmModel } from './models/bdm.model';
import { BdmService } from './bdm.service';
import { BdmController } from './bdm.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([BdmModel], CcrDatabaseStrategy)],
  providers: [BdmService],
  controllers: [BdmController],
})
export class BdmModule {}
