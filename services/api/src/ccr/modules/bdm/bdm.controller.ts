import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { BdmModel } from './models/bdm.model';
import { BdmService } from './bdm.service';

@Controller('bdm')
@ApiTags('BDM')
export class BdmController extends BaseController<BdmModel>() {
  constructor(
    private readonly bdmService: BdmService,
  ) {
    super(bdmService);
  }
}
