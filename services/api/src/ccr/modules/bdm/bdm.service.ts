import { Injectable } from '@nestjs/common';
import { BdmModel } from './models/bdm.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class BdmService extends BaseService<BdmModel>{
  constructor(
    @InjectSequelizeModel(BdmModel)
    private readonly repository: BaseModelStatic<BdmModel>,
  ) {
    super(repository);
  }
}
