import { Injectable } from '@nestjs/common';
import { CostChangeRequestModel } from './models/cost-change-request.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class CostChangeRequestService extends BaseService<CostChangeRequestModel>{
  constructor(
    @InjectSequelizeModel(CostChangeRequestModel)
    private readonly repository: BaseModelStatic<CostChangeRequestModel>,
  ) {
    super(repository);
  }
}
