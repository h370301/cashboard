import {
  CreationOptional,
  DataTypes,
  NonAttribute, Op, sql
} from '@sequelize/core';
import {
  Attribute,
  AutoIncrement,
  BelongsTo,
  HasMany,
  HasOne,
  NotNull,
  PrimaryKey,
  Table
} from '@sequelize/core/decorators-legacy';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { UserModel } from '../../user/models/user.model';
import { SupplierModel } from '../../supplier/models/supplier.model';
import { ReasonForChangeModel } from '../../reason-for-change/models/reason-for-change.model';
import { SelectionModel } from '../../selection/models/selection.model';
import { IngredientAdditionalContextModel } from '../../ingredient-additional-context/models/ingredient-additional-context.model';
import { OtherAdditionalContextModel } from '../../other-additional-context/models/other-additional-context.model';
import { PackagingAdditionalContextModel } from '../../packaging-additional-context/models/packaging-additional-context.model';
import { BdaModel } from '../../bda/models/bda.model';
import { BdmModel } from '../../bdm/models/bdm.model';
import { ContactModel } from '../../contact/models/contact.model';
import { ImpactAndSolutionsModel } from '../../impact-and-solutions/models/impact-and-solutions.model';
import { DealModel } from '../../deal/models/deal.model';
import { FormType } from '../../../shared/constants';

@Table({
  tableName: 'cost_change_requests',
})
export class CostChangeRequestModel extends BaseModel<CostChangeRequestModel> {
  @Attribute(DataTypes.BOOLEAN)
  @NotNull()
  declare abandoned: boolean;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare apNumber: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare displayName: string;

  @Attribute(DataTypes.STRING)
  declare formType: FormType;

  @Attribute(DataTypes.STRING)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  declare updatedAt: CreationOptional<Date>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare userId: string;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare username: string;

  @BelongsTo(() => UserModel, {
    foreignKey: 'id',
    inverse: {
      type: 'hasMany',
      as: 'costChangeRequests',
    },
  })
  declare user?: NonAttribute<UserModel>;

  @HasOne(() => SupplierModel, {
    foreignKey: 'apNumber',
    sourceKey: 'apNumber',
    inverse: {
      as: 'costChangeRequest',
    },
  })
  declare supplier?: SupplierModel;

  @HasMany(() => ReasonForChangeModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest',
    },
  })
  declare reasonsForChange?: NonAttribute<ReasonForChangeModel[]>;

  @HasMany(() => SelectionModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest',
    },
  })
  declare selections?: NonAttribute<SelectionModel[]>;

  @HasMany(() => IngredientAdditionalContextModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest'
    }
  })
  declare ingredientAdditionalContext?: NonAttribute<IngredientAdditionalContextModel[]>;

  @HasMany(() => OtherAdditionalContextModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest'
    }
  })
  declare otherAdditionalContext?: NonAttribute<OtherAdditionalContextModel[]>;

  @HasMany(() => PackagingAdditionalContextModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest'
    }
  })
  declare packagingAdditionalContext?: NonAttribute<PackagingAdditionalContextModel[]>;

  @HasOne(() => BdaModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest'
    }
  })
  declare bda?: NonAttribute<BdaModel>;

  @HasOne(() => BdmModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest'
    }
  })
  declare bdm?: NonAttribute<BdmModel>;

  @HasOne(() => ContactModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest'
    }
  })
  declare contact?: NonAttribute<ContactModel>;

  @HasMany(() => ImpactAndSolutionsModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest'
    }
  })
  declare impactAndSolutions?: NonAttribute<ImpactAndSolutionsModel[]>;

  @HasMany(() => DealModel, {
    foreignKey: 'costJustificationFormId',
    inverse: {
      as: 'costChangeRequest',
    },
  })
  declare deals?: NonAttribute<DealModel[]>;
}
