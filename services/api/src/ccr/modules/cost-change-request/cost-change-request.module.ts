import { Module } from '@nestjs/common';
import { CostChangeRequestModel } from './models/cost-change-request.model';
import { CostChangeRequestService } from './cost-change-request.service';
import { CostChangeRequestController } from './cost-change-request.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([CostChangeRequestModel], CcrDatabaseStrategy)],
  providers: [CostChangeRequestService],
  controllers: [CostChangeRequestController],
})
export class CostChangeRequestModule {}
