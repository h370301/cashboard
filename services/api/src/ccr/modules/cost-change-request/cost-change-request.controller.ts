import { Controller, DefaultValuePipe, Get, ParseIntPipe, Query } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { CostChangeRequestModel } from './models/cost-change-request.model';
import { CostChangeRequestService } from './cost-change-request.service';
import { PaginationDefaults } from '../../../shared/constants/pagination.enum';
import { UserModel } from '../user/models/user.model';
import { DataTypes, Op, sql } from '@sequelize/core';
import { SupplierModel } from '../supplier/models/supplier.model';
import { SelectionModel } from '../selection/models/selection.model';
import { DealModel } from '../deal/models/deal.model';

@Controller('cost-change-request')
@ApiTags('Cost Change Request')
export class CostChangeRequestController extends BaseController<CostChangeRequestModel>() {
  constructor(
    private readonly service: CostChangeRequestService,
  ) {
    super(service);
  }

  @Get('paginate')
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  public paginate(
    @Query(
      'limit',
      new DefaultValuePipe(PaginationDefaults.DEFAULT_LIMIT),
      ParseIntPipe,
    )
    limit: number,
    @Query(
      'page',
      new DefaultValuePipe(PaginationDefaults.DEFAULT_OFFSET),
      ParseIntPipe,
    )
    page: number,
  ): Promise<{ rows: CostChangeRequestModel[], count: number }> {
    const where = { abandoned: false };
    const queryLimit = Math.min(limit, PaginationDefaults.DEFAULT_LIMIT);
    const queryOffset = queryLimit * page;

    return Promise.all([
      this.service.count({ where }),
      this.service.findAll({
        where,
        limit: queryLimit,
        offset: queryOffset,
        include: [
          {
            model: UserModel,
            on: { id: { [Op.eq]: sql.cast(sql.attribute('id'), DataTypes.INTEGER) } },
          },
          SupplierModel, SelectionModel, DealModel,
        ],
      }),
    ])
      .then(([count, rows]) => ({
        rows,
        limit,
        count: rows.length,
        total: count,
        page,
      }));
  }
}
