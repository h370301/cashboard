import { FormType } from '../../../shared/constants';
import { Type } from 'class-transformer';
import { User } from '../../user/dto/user.dto';
import { Timestamped } from '../../../types/dto/timestamped.dto';
import { Supplier } from '../../supplier/dto/supplier.dto';
import { Selection } from '../../selection/dto/selection.dto';
import { Bda } from '../../bda/dto/bda.dto';
import { Bdm } from '../../bdm/dto/bdm.dto';
import { Contact } from '../../contact/dto/contact.dto';
import { ImpactAndSolutions } from '../../impact-and-solutions/dto/impact-and-solutions.dto';
import { Deal } from '../../deal/dto/deal.dto';
import { PackagingAdditionalContext } from '../../packaging-additional-context/dto/packaging-additional-context.dto';
import { OtherAdditionalContext } from '../../other-additional-context/dto/other-additional-context.dto';
import { IngredientAdditionalContext } from '../../ingredient-additional-context/dto/ingredient-additional-context.dto';
import { ReasonForChange } from '../../reason-for-change/dto/reason-for-change.dto';

export class CostChangeRequest extends Timestamped{
  public abandoned: boolean;

  public apNumber: number;

  public displayName: string;

  public formType: FormType;

  public id: number;

  public userId: string;

  public username: string;

  @Type(() => User)
  public user: User;

  @Type(() => Supplier)
  public supplier: Supplier;

  @Type(() => ReasonForChange)
  public reasonsForChange: ReasonForChange[];

  @Type(() => Selection)
  public selections: Selection[];

  @Type(() => IngredientAdditionalContext)
  public ingredientAdditionalContext: IngredientAdditionalContext[];

  @Type(() => OtherAdditionalContext)
  public otherAdditionalContext: OtherAdditionalContext[];

  @Type(() => PackagingAdditionalContext)
  public packagingAdditionalContext: PackagingAdditionalContext[];

  @Type(() => Bda)
  public bda: Bda;

  @Type(() => Bdm)
  public bdm: Bdm;

  @Type(() => Contact)
  public contact: Contact;

  @Type(() => ImpactAndSolutions)
  public impactAndSolutions: ImpactAndSolutions[];

  @Type(() => Deal)
  public deals: Deal[];
}
