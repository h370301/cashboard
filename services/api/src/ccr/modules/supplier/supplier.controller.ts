import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { SupplierModel } from './models/supplier.model';
import { SupplierService } from './supplier.service';

@Controller('supplier')
@ApiTags('supplier')
export class SupplierController extends BaseController<SupplierModel>() {
  constructor(
    private readonly supplierService: SupplierService,
  ) {
    super(supplierService);
  }
}
