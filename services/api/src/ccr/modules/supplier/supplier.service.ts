import { Injectable } from '@nestjs/common';
import { SupplierModel } from './models/supplier.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class SupplierService extends BaseService<SupplierModel>{
  constructor(
    @InjectSequelizeModel(SupplierModel)
    private readonly repository: BaseModelStatic<SupplierModel>,
  ) {
    super(repository);
  }
}
