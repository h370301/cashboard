import { Module } from '@nestjs/common';
import { SupplierModel } from './models/supplier.model';
import { SupplierService } from './supplier.service';
import { SupplierController } from './supplier.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([SupplierModel], CcrDatabaseStrategy)],
  providers: [SupplierService],
  controllers: [SupplierController],
})
export class SupplierModule {}
