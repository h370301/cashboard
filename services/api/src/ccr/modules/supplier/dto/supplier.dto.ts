import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';
import { Type } from 'class-transformer';
import { Timestamped } from '../../../types/dto/timestamped.dto';

export class Supplier extends Timestamped {
  public apDescription: string;

  public apNumber: number;

  @Type(() => CostChangeRequest)
  public costChangeRequest: CostChangeRequest;

  public id: number;

  public userId: number;
}
