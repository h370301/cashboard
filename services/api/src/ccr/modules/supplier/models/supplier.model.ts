import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { CreationOptional, DataTypes, NonAttribute } from '@sequelize/core';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { CostChangeRequestModel } from '../../cost-change-request/models/cost-change-request.model';

@Table({
  tableName: 'ccr_users_suppliers',
})
export class SupplierModel extends BaseModel<SupplierModel> {
  @Attribute(DataTypes.STRING)
  @NotNull()
  declare apDescription: string;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare apNumber: number;

  declare costChangeRequest: NonAttribute<CostChangeRequestModel>;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  declare updatedAt: CreationOptional<Date>;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare userId: number;
}
