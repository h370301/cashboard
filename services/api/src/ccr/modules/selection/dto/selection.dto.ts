import { SelectionType } from '../../../shared/constants';
import { Type } from 'class-transformer';
import { CostChangeRequestModel } from '../../cost-change-request/models/cost-change-request.model';

export class Selection {
  public corporate: boolean;

  @Type(() => CostChangeRequestModel)
  public costChangeRequest: CostChangeRequestModel;

  public costJustificationFormId: number;

  public createdAt: Date;

  public endDate: Date;

  public id: number;

  public proposedListCost: number;

  public selection: string;

  public selectionType: SelectionType;

  public startDate: Date;

  public suggestedRetailPrice: number;

  public universal: boolean;

  public updatedAt: Date;
}
