import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { CreationOptional, DataTypes, NonAttribute } from '@sequelize/core';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { SelectionType } from '../../../shared/constants';
import { CostChangeRequestModel } from '../../cost-change-request/models/cost-change-request.model';

@Table({
  tableName: 'ccr_selections',
})
export class SelectionModel extends BaseModel<SelectionModel> {
  @Attribute(DataTypes.BOOLEAN)
  @NotNull()
  declare corporate: boolean;

  declare costChangeRequest: NonAttribute<CostChangeRequestModel>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare costJustificationFormId: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.DATE)
  declare endDate: Date;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare proposedListCost: number;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare selection: string;

  @Attribute(DataTypes.STRING)
  @NotNull()
  declare selectionType: SelectionType;

  @Attribute(DataTypes.DATE)
  @NotNull()
  declare startDate: Date;

  @Attribute(DataTypes.INTEGER)
  declare suggestedRetailPrice: number;

  @Attribute(DataTypes.BOOLEAN)
  @NotNull()
  declare universal: boolean;

  declare updatedAt: CreationOptional<Date>;
}
