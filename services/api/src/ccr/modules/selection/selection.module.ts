import { Module } from '@nestjs/common';
import { SelectionModel } from './models/selection.model';
import { SelectionService } from './selection.service';
import { SelectionController } from './selection.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([SelectionModel], CcrDatabaseStrategy)],
  providers: [SelectionService],
  controllers: [SelectionController],
})
export class SelectionModule {}
