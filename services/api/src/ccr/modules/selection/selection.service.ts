import { Injectable } from '@nestjs/common';
import { SelectionModel } from './models/selection.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class SelectionService extends BaseService<SelectionModel>{
  constructor(
    @InjectSequelizeModel(SelectionModel)
    private readonly repository: BaseModelStatic<SelectionModel>,
  ) {
    super(repository);
  }
}
