import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { SelectionModel } from './models/selection.model';
import { SelectionService } from './selection.service';

@Controller('selection')
@ApiTags('Selection')
export class SelectionController extends BaseController<SelectionModel>() {
  constructor(
    private readonly selectionService: SelectionService,
  ) {
    super(selectionService);
  }
}
