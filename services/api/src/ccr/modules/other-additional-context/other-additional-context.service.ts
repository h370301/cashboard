import { Injectable } from '@nestjs/common';
import { OtherAdditionalContextModel } from './models/other-additional-context.model';
import { BaseModelStatic } from '../../../shared/http/classes/base.model';
import { BaseService } from '../../../shared/http/classes/base.service';
import { InjectSequelizeModel } from '@cashboard/nest/sequelize';

@Injectable()
export class OtherAdditionalContextService extends BaseService<OtherAdditionalContextModel>{
  constructor(
    @InjectSequelizeModel(OtherAdditionalContextModel)
    private readonly repository: BaseModelStatic<OtherAdditionalContextModel>,
  ) {
    super(repository);
  }
}
