import { Module } from '@nestjs/common';
import { OtherAdditionalContextModel } from './models/other-additional-context.model';
import { OtherAdditionalContextService } from './other-additional-context.service';
import { OtherAdditionalContextController } from './other-additional-context.controller';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from '../../shared/strategies/ccr-database.strategy';

@Module({
  imports: [DatabaseModule.forFeature([OtherAdditionalContextModel], CcrDatabaseStrategy)],
  providers: [OtherAdditionalContextService],
  controllers: [OtherAdditionalContextController],
})
export class OtherAdditionalContextModule {}
