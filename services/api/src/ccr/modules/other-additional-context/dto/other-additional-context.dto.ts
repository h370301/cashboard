import { CostChangeRequest } from '../../cost-change-request/dto/cost-change.request.dto';
import { Type } from 'class-transformer';

export class OtherAdditionalContext {
  public additionalDetail: string;

  public contextType: string;

  @Type(() => CostChangeRequest)
  public costChangeRequest: CostChangeRequest;

  public costJustificationFormId: number;

  public createdAt: Date;

  public dateExperienced: Date;

  public id: number;

  public selectionId: number;

  public updatedAt: Date;
}
