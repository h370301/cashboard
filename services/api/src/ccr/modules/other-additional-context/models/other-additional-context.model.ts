import { CreationOptional, DataTypes, NonAttribute } from '@sequelize/core';
import { Attribute, AutoIncrement, NotNull, PrimaryKey, Table } from '@sequelize/core/decorators-legacy';
import { BaseModel } from '../../../../shared/http/classes/base.model';
import { CostChangeRequestModel } from '../../cost-change-request/models/cost-change-request.model';

@Table({
  tableName: 'ccr_additional_context_other',
})
export class OtherAdditionalContextModel extends BaseModel<OtherAdditionalContextModel> {
  @Attribute(DataTypes.STRING)
  declare additionalDetail: string;

  @Attribute(DataTypes.STRING)
  declare contextType: string;

  declare costChangeRequest: NonAttribute<CostChangeRequestModel>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare costJustificationFormId: number;

  declare createdAt: CreationOptional<Date>;

  @Attribute(DataTypes.DATE)
  declare dateExperienced: Date;

  @Attribute(DataTypes.INTEGER)
  @PrimaryKey()
  @AutoIncrement()
  declare id: CreationOptional<number>;

  @Attribute(DataTypes.INTEGER)
  @NotNull()
  declare selectionId: number;

  declare updatedAt: CreationOptional<Date>;
}
