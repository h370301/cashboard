import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../../shared/http/classes/base.controller';
import { OtherAdditionalContextModel } from './models/other-additional-context.model';
import { OtherAdditionalContextService } from './other-additional-context.service';

@Controller('other-additional-context')
@ApiTags('Other Additional Context')
export class OtherAdditionalContextController extends BaseController<OtherAdditionalContextModel>() {
  constructor(
    private readonly otherAdditionalContextService: OtherAdditionalContextService,
  ) {
    super(otherAdditionalContextService);
  }
}
