export enum SelectionType {
  COST_LINK = 'costLink',
  DSD_UPC = 'dsdUpc',
  ITEM_CODE = 'itemCode'
}
