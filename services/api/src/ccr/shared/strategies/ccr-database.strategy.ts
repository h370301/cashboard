import { SequelizeStrategy } from '@cashboard/nest/sequelize';
import { BdaModel } from '../../modules/bda/models/bda.model';
import { BdmModel } from '../../modules/bdm/models/bdm.model';
import { ContactModel } from '../../modules/contact/models/contact.model';
import { DealModel } from '../../modules/deal/models/deal.model';
import { ImpactAndSolutionsModel } from '../../modules/impact-and-solutions/models/impact-and-solutions.model';
import {
  ImpactAndSolutionsFactorModel
} from '../../modules/impact-and-solutions-factor/models/impact-and-solutions-factor.model';
import {
  IngredientAdditionalContextModel
} from '../../modules/ingredient-additional-context/models/ingredient-additional-context.model';
import { ItemDetailWarehouseModel } from '../../modules/item-detail-warehouse/models/item-detail-warehouse.model';
import {
  OtherAdditionalContextModel
} from '../../modules/other-additional-context/models/other-additional-context.model';
import {
  PackagingAdditionalContextModel
} from '../../modules/packaging-additional-context/models/packaging-additional-context.model';
import { ReasonForChangeModel } from '../../modules/reason-for-change/models/reason-for-change.model';
import { SelectionModel } from '../../modules/selection/models/selection.model';
import { SupplierModel } from '../../modules/supplier/models/supplier.model';
import { UserModel } from '../../modules/user/models/user.model';
import { CostChangeRequestModel } from '../../modules/cost-change-request/models/cost-change-request.model';

export const CcrDatabaseStrategy = new SequelizeStrategy({
  database: {
    dialect: 'postgres',
    dialectModule: require('pg'),
    host: process.env['CCR_DATABASE_HOST'] || 'localhost',
    port: parseInt(<string>process.env['CCR_DATABASE_PORT']) || 5432,
    username: process.env['CCR_DATABASE_USERNAME'],
    password: process.env['CCR_DATABASE_PASSWORD'],
    database: process.env['CCR_DATABASE_DATABASE'],
    models: [
      BdaModel,
      BdmModel,
      ContactModel,
      CostChangeRequestModel,
      DealModel,
      ImpactAndSolutionsModel,
      ImpactAndSolutionsFactorModel,
      IngredientAdditionalContextModel,
      ItemDetailWarehouseModel,
      OtherAdditionalContextModel,
      PackagingAdditionalContextModel,
      ReasonForChangeModel,
      SelectionModel,
      SupplierModel,
      UserModel,
    ],
  },
})
