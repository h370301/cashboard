import { Module } from '@nestjs/common';
import { BdaModule } from './modules/bda/bda.module';
import { BdmModule } from './modules/bdm/bdm.module';
import { ContactModule } from './modules/contact/contact.module';
import { DealModule } from './modules/deal/deal.module';
import { ImpactAndSolutionsModule } from './modules/impact-and-solutions/impact-and-solutions.module';
import {
  ImpactAndSolutionsFactorModule
} from './modules/impact-and-solutions-factor/impact-and-solutions-factor.module';
import { IngredientAdditionalContextModule } from './modules/ingredient-additional-context/ingredient-additional-context.module';
import { ItemDetailWarehouseModule } from './modules/item-detail-warehouse/item-detail-warehouse.module';
import { OtherAdditionalContextModule } from './modules/other-additional-context/other-additional-context.module';
import { PackagingAdditionalContextModule } from './modules/packaging-additional-context/packaging-additional-context.module';
import { ReasonForChangeModule } from './modules/reason-for-change/reason-for-change.module';
import { SelectionModule } from './modules/selection/selection.module';
import { SupplierModule } from './modules/supplier/supplier.module';
import { UserModule } from './modules/user/user.module';
import { DatabaseModule } from '@cashboard/nest/database';
import { CcrDatabaseStrategy } from './shared/strategies/ccr-database.strategy';
import { CostChangeRequestModule } from './modules/cost-change-request/cost-change-request.module';

@Module({
  imports: [
    DatabaseModule.forRoot({
      strategy: CcrDatabaseStrategy,
    }),
    BdaModule,
    BdmModule,
    ContactModule,
    CostChangeRequestModule,
    DealModule,
    ImpactAndSolutionsModule,
    ImpactAndSolutionsFactorModule,
    IngredientAdditionalContextModule,
    ItemDetailWarehouseModule,
    OtherAdditionalContextModule,
    PackagingAdditionalContextModule,
    ReasonForChangeModule,
    SelectionModule,
    SupplierModule,
    UserModule,
  ],
  exports: [
    BdaModule,
    BdmModule,
    ContactModule,
    CostChangeRequestModule,
    DealModule,
    ImpactAndSolutionsModule,
    ImpactAndSolutionsFactorModule,
    IngredientAdditionalContextModule,
    ItemDetailWarehouseModule,
    OtherAdditionalContextModule,
    PackagingAdditionalContextModule,
    ReasonForChangeModule,
    SelectionModule,
    SupplierModule,
    UserModule,
  ],
})
export class CcrModule {}
