import { Exclude } from 'class-transformer';

export class Timestamped {
  @Exclude()
  public createdAt: Date;

  @Exclude()
  public updatedAt: Date;
}
