import axios, { AxiosInstance, AxiosPromise, AxiosRequestConfig } from 'axios';
import { UrlFormattingInterceptor } from './url-formatter';

type WithRequired<
  T extends object,
  K extends keyof T,
> =  Required<Pick<T, K>> & Omit<T, K>;

type WithUppercase<T extends string> = T extends (infer U extends string)
  ? (U | Uppercase<U>)
  : never;

type ApiMethod = WithUppercase<'get' | 'delete' | 'head' | 'options' | 'post' | 'put' | 'patch'>;

type ApiMethodHandler = (config?: Partial<AxiosRequestConfig>) => AxiosPromise;

type ResourceInstance<Methods extends string> = { [Method in Methods]: ApiMethodHandler };

/**
 * The object schema to build a Resource
 *
 * Notes about `ejectKey`:
 *
 * Assuming the HTTP Request returns `{ data: [] }
 *
 * if ejectKey = 'data', the function will return `[]`
 *
 * if the ejectKey is not set, the function will return `{ data: [] }`
 *
 * if the ejectKey is `badKey`, the function will return undefined
 */
type ApiMethodSchema<Method = ApiMethod> = {
  method: Method;
  url?: string;
  /**
   * The attribute to eject from the HTTP Response object
   */
  ejectKey?: string;
};

type ResourceSchema<Methods extends string> = { [K in Methods]: ApiMethodSchema };

/**
 * A Factory class that builds Resources that live at a given HTTP endpoint
 *
 * @example
 * const builder = new ResourceFactory({
 *   baseURL: 'https://www.test.com',
 *   headers: { apiKey: '123456' }
 * });
 *
 * const products = builder.build('/v1/api', {
 *   search: {
 *     method: 'post',
 *     url: '/{formType}/search',
 *   },
 * });
 *
 * // send a request to https://www.test.com/v1/api/warehouse/search
 * products.search({
 *   params: { formType: 'warehouse' },
 * })
 * .then((res: AxiosResponse<any>) => {
 *   // do things
 * });
 */
export class ResourceFactory {
  public readonly axiosInstance: AxiosInstance;

  constructor(
    { headers = {}, ...restConfig }: WithRequired<AxiosRequestConfig, 'baseURL'>,
  ) {
    const config = {
      ...restConfig,
      headers: {
        ...headers,
        'Accept': headers['Accept'] || 'application/json',
      },
    };

    this.axiosInstance = axios.create(config);
    this.axiosInstance.interceptors.request.use(UrlFormattingInterceptor);
  }

  /**
   * Builds a single request with the AxiosInstance
   *
   * @param baseURL the baseURL in AxiosRequestConfig
   * @param method the method in AxiosRequestConfig
   * @param url the url in AxiosRequestConfig
   * @param ejectKey utility key to expose an inner attribute
   */
  private buildRequest<Method extends string>(
    baseURL: string,
    { method, url = '', ejectKey }: ApiMethodSchema<Method>,
  ): ApiMethodHandler {
    return (requestConfig: Partial<AxiosRequestConfig> = {}) => {
      return new Promise((resolve, reject) => {
        this.axiosInstance.request({
          ...requestConfig,
          baseURL,
          method,
          url,
        })
          .then((response) => {
            resolve(ejectKey ? response[ejectKey] : response);
          })
          .catch((error) => {
            reject(error);
          });
      });
    };
  }

  /**
   * Build a simple resource
   *
   * @param baseUrl the baseUrl that all schema methods will append to
   * @param schema the available methods for this resource
   */
  public build<Methods extends string>(
    baseUrl: string,
    schema: ResourceSchema<Methods>,
  ): ResourceInstance<Methods> {
    const resource = {} as ResourceInstance<Methods>;
    const methods = Object.keys(schema) as Methods[];

    for (const methodName of methods) {
      resource[methodName] = this.buildRequest(baseUrl, schema[methodName]);
    }

    return resource;
  }
}
