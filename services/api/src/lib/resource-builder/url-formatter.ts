/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from 'axios';

/**
 * A very rough format method that replaces mustaches in the URL with params from
 * the AxiosRequestConfig passed on execute
 *
 * Analyzes the `config.params` object and checks for occurences of the key
 * the url for the request and then replaces them
 *
 *        format: /banana/{id}/pudding/{name}
 * config.params: { id: 12, name: 'jello' }
 *       results: /banana/12/pudding/jello
 */
export const UrlFormattingInterceptor = (config: axios.InternalAxiosRequestConfig): axios.InternalAxiosRequestConfig => {
  const { params, url } = config;

  if (!params || !url) {
    return config;
  }

  const configKeys = Object.keys(config.params);

  for (const paramKey of configKeys) {
    const urlParam = configKeys[paramKey];
    const lookupString = `{${paramKey}}`;

    if (url.indexOf(lookupString) > -1) {
      config.url = url.replace(lookupString, urlParam);
      delete config.params[paramKey];
    }
  }

  return config;
};
