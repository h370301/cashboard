export enum PaginationDefaults {
  DEFAULT_LIMIT = 10,
  MAX_LIMIT = 20,
  DEFAULT_OFFSET = 0,
}
