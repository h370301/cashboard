export interface IBaseService<M> {
  findAll(): Promise<M[]>;
  paginate(limit: number, offet: number): Promise<{ rows: M[], count: number }>;
  findOne(id: number | string): Promise<M>;
}
