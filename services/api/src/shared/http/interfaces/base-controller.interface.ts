import { BaseModel } from '../classes/base.model';

export interface IBaseController<M extends BaseModel> {
  findAll(): Promise<M[]>;
  paginate(limit: number, offset: number): Promise<{ rows: M[], count: number }>;
  findOne(id: number | string): Promise<M>;
}
