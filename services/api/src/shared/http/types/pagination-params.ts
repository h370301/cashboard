import { ApiPropertyOptional } from '@nestjs/swagger';

export class PaginationParams {
  @ApiPropertyOptional({
    type: Number,
    default: 10,
  })
  public limit;
  @ApiPropertyOptional({
    type: Number,
    default: 0,
  })
  public page = 0;
}
