import { Attributes, CountOptions, FindOptions, WhereOptions } from '@sequelize/core';
import { IBaseService } from '../interfaces/base-service.interface';
import { BaseModel, BaseModelStatic } from './base.model';
import { PaginationDefaults } from '../../constants/pagination.enum';

export const DEFAULT_PAGE_SIZE = 10;

export abstract class BaseService<M extends BaseModel>
  implements IBaseService<M>
{
  protected DEFAULT_PAGE_SIZE = DEFAULT_PAGE_SIZE;
  constructor(
    private readonly model: BaseModelStatic<M>,
  ) {}

  public count(
    options?: CountOptions<Attributes<M>>
  ): Promise<number> {
    return this.model.count(options);
  }

  public findAll(
    options?: FindOptions<Attributes<M>>
  ): Promise<M[]> {
    return this.model.findAll(options);
  }

  public paginate(
    limit: number = this.DEFAULT_PAGE_SIZE,
    page: number,
    options?: FindOptions<Attributes<M>>,
  ): Promise<{ rows: M[], count: number }> {
    const queryLimit = Number(limit) || PaginationDefaults.DEFAULT_LIMIT;
    const queryOffset = Number(page) || PaginationDefaults.DEFAULT_OFFSET;

    return this.model.findAndCountAll({
      ...options,
      limit: Math.min (queryLimit, PaginationDefaults.MAX_LIMIT),
      offset: queryOffset * queryLimit,
    });
  }

  public findOne(
    id: number | string,
    options?: Exclude<FindOptions<Attributes<M>>, WhereOptions<Attributes<M>>>,
  ): Promise<M> {
    return this.model
      .findOne({
        ...options,
        where: { id },
      });
  }
}
