/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Model as SequelizeModel,
  ModelStatic as SequelizeModelStatic,
} from '@sequelize/core';
import { InferAttributes, InferCreationAttributes } from '@sequelize/core';
import { Table } from '@sequelize/core/decorators-legacy';

export type BaseModelStatic<
  Model extends SequelizeModel = any,
> = SequelizeModelStatic<Model>;

@Table.Abstract({
  underscored: true,
})
export class BaseModel<
  M extends BaseModel<M> = BaseModel<any>,
> extends SequelizeModel<InferAttributes<M>, InferCreationAttributes<M>> {}
