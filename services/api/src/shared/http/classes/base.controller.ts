import { BaseModel } from './base.model';
import {
  ClassSerializerInterceptor,
  DefaultValuePipe,
  Get,
  Param,
  ParseIntPipe,
  Query,
  Type,
  UseInterceptors
} from '@nestjs/common';
import { IBaseController } from '../interfaces/base-controller.interface';
import { IBaseService } from '../interfaces/base-service.interface';
import { ApiParam, ApiQuery } from '@nestjs/swagger';
import { PaginationDefaults } from '../../constants/pagination.enum';

export function BaseController<M extends BaseModel>(): Type<IBaseController<M>> {
  class GenericsController<M extends BaseModel>
    implements IBaseController<M>
  {
    constructor(
      private readonly service: IBaseService<M>,
    ) {}

    @Get()
    @UseInterceptors(ClassSerializerInterceptor)
    public findAll(): Promise<M[]> {
      return this.service.findAll();
    }

    @Get('paginate')
    @UseInterceptors(ClassSerializerInterceptor)
    @ApiQuery({ name: 'limit', type: Number, required: false })
    @ApiQuery({ name: 'offset', type: Number, required: false })
    public paginate(
      @Query(
        'limit',
        new DefaultValuePipe(PaginationDefaults.DEFAULT_LIMIT),
        ParseIntPipe,
      )
      limit: number,
      @Query(
        'page',
        new DefaultValuePipe(PaginationDefaults.DEFAULT_OFFSET),
        ParseIntPipe,
      )
      page: number,
    ): Promise<{ rows: M[], count: number }> {
      return this.service.paginate(+limit, +page)
        .then(({ rows, count }) => ({
          rows,
          limit,
          count: rows.length,
          total: count,
          page,
        }))
    }

    @ApiParam({ name: 'id' })
    @UseInterceptors(ClassSerializerInterceptor)
    @Get(':id')
    public findOne(
      @Param('id') id: number | string,
    ): Promise<M> {
      return this.service.findOne(id);
    }
  }

  return GenericsController;
}
