import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CcrModule } from '../ccr/ccr.module';
import { RouterModule } from '@nestjs/core';

@Module({
  imports: [
    CcrModule,
    RouterModule.register([
      {
        path: 'ccr',
        module: CcrModule,
      },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
